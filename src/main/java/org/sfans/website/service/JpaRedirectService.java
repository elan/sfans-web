package org.sfans.website.service;

import java.util.Optional;

import org.sfans.domain.Redirect;
import org.sfans.domain.RedirectRepository;
import org.sfans.domain.Website;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.stereotype.Service;

@Profile("web")
@Service
public class JpaRedirectService implements RedirectService {

	@Autowired
	private RedirectRepository repository;

	@Override
	public Optional<Redirect> findRedirect(final Website website, final String url) {
		return repository.findActive(website, url).stream().findFirst();
	}

}
