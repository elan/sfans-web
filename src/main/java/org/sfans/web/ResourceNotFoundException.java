package org.sfans.web;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;


@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class ResourceNotFoundException extends RuntimeException
{
	private static final long serialVersionUID = 1790696317383743536L;

	public ResourceNotFoundException(final String message)
    {
        super(message);
    }

    public ResourceNotFoundException()
    {
    	super();
    }
}
